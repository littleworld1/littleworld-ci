[![pipeline status](https://gitlab.com/littleworld1/littleworld-ci/badges/main/pipeline.svg)](https://gitlab.com/littleworld1/littleworld-ci/-/commits/main)

# LittleWorld-ci

## Infrastructure

![resources](https://docs.google.com/drawings/d/1STM2pElZbFf0-lImZ4SBPPFWLRm1DpaEEtz58lbnQl4/export/png)

[Google drawings](https://docs.google.com/drawings/d/1STM2pElZbFf0-lImZ4SBPPFWLRm1DpaEEtz58lbnQl4/edit?usp=sharing)

## Docker

### Registering gitlab runner

```
docker volume create gitlab-runner-config
docker run --rm -it -v gitlab-runner-config:/etc/gitlab-runner gitlab/gitlab-runner:latest register
```

### Start/update gitlab runner

```
docker stop gitlab-runner && docker rm gitlab-runner
docker pull gitlab/gitlab-runner
docker run -d --name gitlab-runner --restart always \
    -v /var/run/docker.sock:/var/run/docker.sock \
    -v gitlab-runner-config:/etc/gitlab-runner \
    --env TZ=Europe/Moscow \
    gitlab/gitlab-runner:latest
```

### Delete gitlab runner

```
docker stop gitlab-runner && docker rm gitlab-runner
```

### Docker image with jq and curl

```
FROM debian:10.10-slim

RUN apt-get update && \
    apt-get -y upgrade && \
    apt-get install --no-install-recommends -y curl jq && \
    apt-get clean && \
    rm -rf /var/lib/apt/lists/*
```

